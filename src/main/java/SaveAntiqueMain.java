import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Scanner;

public class SaveAntiqueMain {

    public static Client client = ClientBuilder.newClient();
    public static Antique newAntique = new Antique();
    public static Scanner scanner = new Scanner(System.in);

    /**
     * main method presenting a choice of show antiques list
     * add antique
     * and exit the program
     * @param args
     */
    public static void main(String[] args) {

        boolean running1 = true;
        while(running1){
            System.out.println("1 - Show a list of all antiques\n2 - Add new antique\n3 - Exit ");
            int command = scanner.nextInt();
            switch (command) {
                case 1: {
                    getAntiquesList();
                    break;
                }
                case 2: {
                    addNewAntique();
                    break;
                }
                case 3: {
                    running1 = false;
                    break;
                }
            }
        }
    }

    /**
     * method getting a list of all country object from the database through REST API
     * @return
     */
    public static List<Country> getCountriesList () {
        WebTarget target = client.target("http://localhost:8080/Homework6-1.0-SNAPSHOT/api/countries");
        Invocation.Builder request = target.request();
        Response response = request.get();
        List<Country> countriesList = response.readEntity(new GenericType<List<Country>>() {});
        return countriesList;
    }

    /**
     * method invoking a persist method on an antique, through a REST API
     * @param antique
     */
    public static void saveAntique (Antique antique) {
        WebTarget targetPost = client.target("http://localhost:8080/Homework6-1.0-SNAPSHOT/api/antiques/save");//tworzymy nowy target z restem do zapisywania
        Invocation.Builder postRequest = targetPost.request(); //tworzymy post request
        postRequest.post(Entity.json(antique)); //za pomoca post requesta wysylamy obiekt jsona
    }

    /**
     * method used to print out all the countirs to the console
     * @param countries
     */
    public static void printOutCountries (List<Country> countries) {
        for (Country country : countries) {
            System.out.println(country.getId()+" - "+ country.getName());

        }
    }

    /**
     * method returning a country by browsing a list by the searched country id
     * @param id - id of the searched country
     * @param countries list to be searched
     * @return country with the specified id
     */
    public static Country getCountryById (int id, List<Country> countries) {
        return  countries.stream().filter(p -> p.getId()==id).findFirst().orElse(null);
    }

    /**
     * Method printing out AVAILABLE/UNAVAILABLE to the console
     */
    public static void printAvailabilities () {
        for (Antique.Availability availability: Antique.Availability.values()) {
            System.out.println(availability.getIndex()+" - " + availability); // ZMIENIC ZEBY DOBRZE ITEROWALO
        }
    }

    /**
     * method asking the user for all the neccessary inputs to create a new antique, crating this antique
     * and then giving the user the option to persist it to the database
     */
    public static void addNewAntique () {
        boolean running = true;

        while (running) {
            System.out.println("Do you want to add a new antique? \n1 - YES\n2 - NO");
            int command = scanner.nextInt();
            switch (command) {
                case 1: {
                    scanner.skip("\n");
                    System.out.println("Please provide a name: ");
                    String name = scanner.nextLine();
                    newAntique.setName(name);

                    System.out.println("Please provide production date: ");
                    newAntique.setProductionDate(scanner.nextInt());
                    scanner.skip("\n");

                    System.out.println("Select origin country: ");
                    List<Country> countries = getCountriesList();
                    printOutCountries(countries);
                    int id = scanner.nextInt();
                    Country country = getCountryById(id, countries);
                    newAntique.setOriginCountry(country);
                    scanner.skip("\n");

                    System.out.println("Please provide a price for the antique: ");
                    int price = scanner.nextInt();
                    newAntique.setPrice(price);
                    scanner.skip("\n");

                    System.out.println("Set the availability: ");
                    printAvailabilities();
                    int ava = scanner.nextInt();
                    if (ava == 1) {
                        newAntique.setAvailability(Antique.Availability.AVAILABLE);
                    } else if (ava == 2) {
                        newAntique.setAvailability(Antique.Availability.UNAVAILABLE);
                    }

                    System.out.println("Do you want to save the antique?\n1 - YES\n2 - NO");
                    int choice = scanner.nextInt();
                    if (choice == 1) {
                        saveAntique(newAntique);
                    } else if (choice == 2) {
                        System.out.println("Antique not saved");
                        break;
                    }
                    break;
                }

                case 2: {
                    running = false;
                    break;
                }
            }
        }
    }

    /**
     * Method used to get all the antiques from the database, through a REST API and print them to console
     */
    public static void getAntiquesList () {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/Homework6-1.0-SNAPSHOT/api/antiques");
        Invocation.Builder request = target.request();
        Response response = request.get();
        List<Antique> antiques = response.readEntity(new GenericType<List<Antique>>(){});
        for (Antique antique:antiques) {
            System.out.println(antique);
        }
        System.out.println("\n");
    }
}
