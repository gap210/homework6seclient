import java.util.Objects;

public class Antique {

    private String name;

    private int productionDate;

    private Country originCountry;

    private int price;

    private Availability availability;

    public enum Availability {
        AVAILABLE(1),
        UNAVAILABLE(2);

        private final int index;

        Availability(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }
    private int id;

    public Antique() {
    }

    public Antique(String name, int productionDate, Country originCountry, int price, Availability availability) {
        this.name = name;
        this.productionDate = productionDate;
        this.originCountry = originCountry;
        this.price = price;
        this.availability = availability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(int productionDate) {
        this.productionDate = productionDate;
    }

    public Country getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(Country originCountry) {
        this.originCountry = originCountry;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Antique antique = (Antique) o;
        return productionDate == antique.productionDate &&
                price == antique.price &&
                id == antique.id &&
                Objects.equals(name, antique.name) &&
                Objects.equals(originCountry, antique.originCountry) &&
                availability == antique.availability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, productionDate, originCountry, price, availability, id);
    }

    @Override
    public String toString() {
        return "Antique{" +
                "name='" + name + '\'' +
                ", productionDate=" + productionDate +
                ", originCountry=" + originCountry +
                ", price=" + price +
                ", availability=" + availability +
                ", id=" + id +
                '}';
    }
}
